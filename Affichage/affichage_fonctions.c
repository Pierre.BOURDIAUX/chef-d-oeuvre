#include "./affichage_fonctions.h"


SDL_Texture * fondEcran = NULL;
SDL_Rect perso[5];
SDL_Texture * persoTexture = NULL;
int persoIndex = 0;
bool persoLoaded = false;
int nbframe = 0;

bool initSDL(SDL_Window ** window, SDL_Renderer ** renderer)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return false;
    }

    *window = SDL_CreateWindow("Meilleur chemin", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280 , 720, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return false;
    }

    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL)
    {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

void afficheFond(SDL_Renderer * renderer)
{
    if(renderer == NULL) printf("Renderer is NULL\n");
    //if(fondEcran == NULL)fondEcran = loadTexture("../img/Fond_Mine.png", renderer);
    if(fondEcran == NULL)fondEcran = loadTexture("../img/mine.png", renderer);
    SDL_RenderCopy(renderer, fondEcran, NULL, NULL);

}

SDL_Texture * loadTexture(char * path, SDL_Renderer * renderer)
{
    SDL_Texture *texture = IMG_LoadTexture(renderer,path);
    if (texture == NULL) printf("Texture is NULL\n"); 
    return texture;
}

void DrawMinerals(SDL_Renderer * renderer, int ** Graph ,SDL_Rect * rect, int N)
{
    SDL_Texture* texture[3];
    texture[0] = loadTexture("../img/fer.png", renderer);
    texture[1] = loadTexture("../img/or.png", renderer);
    texture[2] = loadTexture("../img/diamant.png", renderer);
    for(int i = 0; i < N; i++){
        if(Graph[i][i] == 1) SDL_RenderCopy(renderer, texture[0], NULL, &rect[i]);
        else if(Graph[i][i] == 2) SDL_RenderCopy(renderer, texture[1], NULL, &rect[i]);
        else if(Graph[i][i] == 3) SDL_RenderCopy(renderer, texture[2], NULL, &rect[i]);
        //SDL_RenderDrawRect(renderer, &rect[i]);
    } 
}

// void DrawPerso(SDL_Renderer * renderer, int x, int y)
// {
//     SDL_Rect source={0}, dest;

//     // SDL_Rect dest = {x, y, 100, 100};
//     if(renderer == NULL) printf("Renderer is NULL\n");
//     if(perso == NULL)perso = loadTexture("../img/perso.png", renderer);
//     if(perso == NULL) printf("Perso is NULL\n");

//     SDL_QueryTexture(perso, NULL, NULL, &source.w, &source.h); 

//     dest.x = x;
//     dest.y = y;
//     dest.w =250;
//     dest.h = 250;

//     SDL_RenderCopy(renderer, perso,&source,&dest);

// }

void DrawPerso(SDL_Renderer * renderer, int x, int y)
{
    int window_w = 1280, window_h = 720;
    
    int zoom = 1, nb_images = 5;
    //int offset_x = source.w / nb_images,
    int offset_x = 188,
    offset_y = 275;
    SDL_Rect destination={0};
    SDL_Rect source={0};

    destination.w = offset_x * zoom;       
    destination.h = offset_y * zoom;
    destination.y = y;
    destination.x = x;

    if(!persoLoaded){
        SDL_Rect source={0};
        if(persoTexture == NULL)persoTexture = loadTexture("../img/perso.png", renderer);

        SDL_QueryTexture(persoTexture, NULL, NULL, &source.w, &source.h); 
        int nb_images = 5;                    
 

        for(int i=0; i<nb_images; i++){
            perso[i].x = i*offset_x;
            perso[i].y = 0;
            perso[i].w = offset_x;
            perso[i].h = offset_y;
        }

        persoLoaded = true;
    }

    source.x = perso[persoIndex].x;
    source.y = perso[persoIndex].y;
    source.w = perso[persoIndex].w;
    source.h = perso[persoIndex].h;
    SDL_RenderCopy(renderer, persoTexture, &source, &destination);
   //SDL_RenderDrawRect(renderer, &destination);
    nbframe++;
    if(nbframe == 2)   {
        persoIndex = (persoIndex + 1) % nb_images;
        nbframe = 0;
    }
    SDL_Delay(10);

}

void afficheResJoueur(SDL_Renderer* renderer, SDL_Window * window, int score){
    TTF_Init();
    SDL_RenderClear(renderer);


    SDL_Color color = {255, 255, 255};
    TTF_Font * font = TTF_OpenFont("../font/RobotoSlab-Medium.ttf", 40);
    if(font == NULL){
        printf("Fonts is NULL\n");
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    char str[2048];
    sprintf(str, "Votre score : %d ", score);
    SDL_Surface * surface = TTF_RenderText_Solid(font, str, color);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    SDL_Rect rect;
    rect.x = 300;
    rect.y = 300;
    rect.w = 300;
    rect.h = 50;



    char str2[2048];
    sprintf(str2, "l'ordinateur va vous montrer un des chemins possible");
    SDL_Surface * surface2 = TTF_RenderText_Solid(font, str2, color);
    SDL_Texture * texture2 = SDL_CreateTextureFromSurface(renderer, surface2);
    SDL_FreeSurface(surface2);
    SDL_Rect rect2;
    rect2.x = 300;
    rect2.y = 350;
    rect2.w = 800;
    rect2.h = 50;


    SDL_RenderCopy(renderer, texture2, NULL, &rect2);
    SDL_RenderCopy(renderer, texture, NULL, &rect);
    SDL_RenderPresent(renderer);
    SDL_Delay(3000);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_DestroyTexture(texture);


}

void afficheResCOM(SDL_Renderer* renderer, SDL_Window * window, int score){
    TTF_Init();
    SDL_RenderClear(renderer);


    SDL_Color color = {255, 255, 255};
    TTF_Font * font = TTF_OpenFont("../font/RobotoSlab-Medium.ttf", 40);
    if(font == NULL){
        printf("Fonts is NULL\n");
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    char str[2048];
    sprintf(str, "Score de l'ordinateur : %d ", score);
    SDL_Surface * surface = TTF_RenderText_Solid(font, str, color);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    SDL_Rect rect;
    rect.x = 300;
    rect.y = 300;
    rect.w = 300;
    rect.h = 50;



    

    SDL_RenderCopy(renderer, texture, NULL, &rect);
    SDL_RenderPresent(renderer);
    SDL_Delay(3000);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_DestroyTexture(texture);
}

