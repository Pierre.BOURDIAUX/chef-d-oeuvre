#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

bool initSDL(SDL_Window ** window, SDL_Renderer ** renderer);
void afficheFond(SDL_Renderer * renderer);

SDL_Texture * loadTexture(char * path, SDL_Renderer * renderer);
void DrawMinerals(SDL_Renderer * renderer, int ** Graph ,SDL_Rect * rect, int N);
void DrawPerso(SDL_Renderer * renderer, int x, int y);
void afficheResJoueur(SDL_Renderer* renderer, SDL_Window * window, int score);
