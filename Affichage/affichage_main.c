#include "affichage_fonctions.h"


int main(int argc, char const *argv[])
{
    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    initSDL(&window, &renderer);
    afficheFond(renderer);

    SDL_RenderPresent(renderer);   

    SDL_Delay(3000);
    return 0;
}
