#include "floyd_warshall.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Fonction qui prend en entrée un graphe et qui retourne la matrice des distances entre les sommets du graphe
 * 
 * @param graphe  
 * @return int ** distance
 */
int **floyd_warshall(int ** matrice_adjacence_graphe,int N)
{

    int ** distance = malloc(N * sizeof(int *));
    for (int i = 0; i < N; i++)
    {
        distance[i] = malloc(N * sizeof(float));
    }
    for (int i = 0; i < N; i++)
    {
        distance[i][i] = 0.0;
        for (int j = i + 1; j < N; j++)
        {
            if (matrice_adjacence_graphe[i][j] == 1)
            {
                distance[i][j] = 1;
                distance[j][i] = 1;
            }
            else
            {
                distance[i][j] = 150000;
                distance[j][i] = 150000;
            }
        }
    }
    for (int k = 0; k < N; k++)
    {
        for (int i = 0; i < N; i++)
        {   
            for (int j = 0; j < N; j++)
            {
                if (distance[i][j] > distance[i][k] + distance[k][j])
                {
                    distance[i][j] = distance[i][k] + distance[k][j];
                }
            }
        }
    }
    return distance;
}

