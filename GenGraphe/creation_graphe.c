#include"../ParcoursGraphe/floyd_warshall.h"
#include "./creation_graphe.h"

int **genere(int **matrice,int N, int bas, int haut)
{


    if (bas < haut)
    {
        int k = rand() % (haut - bas) + bas;
        matrice[bas][bas + 1] = 1;
        matrice[bas + 1][bas] = 1;
        if (k + 1 <= haut)
        {
            matrice[bas][k + 1] = 1;
            matrice[k + 1][bas] = 1;
        }
        genere(matrice,N,bas + 1, k);
        genere(matrice,N,k + 1, haut);
    }
    return matrice;
}


int **genereGraphe(int **matrice,int N,float p)
{

    printf("GENERE Graphe N : %d\n", N);
    p=p*100 ; // pourcentage
    for (int i = 0; i < N; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
            if (rand() % 100 < p)
            {
                matrice[i][j] = 1;
                matrice[j][i] = 1;
            }
        }
    }
    return matrice;
}

int **genereGrapheComplet(int **matrice,int N)
{
    int **distance = floyd_warshall(matrice,N);
    printf("GENERE Graphe N : %d\n", N);
    for (int i = 0; i < N; i++)
    {
        for (int j = i + 1; j < N; j++)
        {    
            if (distance[i][j] == 0)
            {
                matrice[i][j] = distance[i][j];
                matrice[j][i] = distance[i][j];
            }
        }
    }
    return matrice;
}

void afficheMatrice(int** matrice, int N)
{
    
    for (int i = 0; i < N; i++)
    {
        printf("[");
        for (int j = 0; j < N; j++)
        {
            printf("%d", matrice[i][j]);
            if (j < N - 1)
            {
                printf(", ");
            }
        }
        printf("]\n");
    }
}

int **creationMatrice(int N)
{
    int **matrice = malloc(N * sizeof(int *));
    for (int i = 0; i < N; i++)
    {
        matrice[i] = malloc(N * sizeof(int));
        for (int j = 0; j < N; j++)
        {
            matrice[i][j] = 0;
        }
    }
    return matrice;
}



void freeMatrice(int **matrice, int N)
{

    for (int i = 0; i < N; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}






