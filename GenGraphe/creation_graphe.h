#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

int **genere(int **matrice, int N, int bas, int haut);
int **genereGraphe(int **matrice,int N,float p);
void afficheMatrice(int ** matrice,int N);
int **creationMatrice(int N);
void freeMatrice(int **matrice,int N);

