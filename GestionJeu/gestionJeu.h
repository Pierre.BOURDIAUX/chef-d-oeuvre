#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "../GenGraphe/creation_graphe.h"
#include "../Affichage/affichage_fonctions.h"

#define N 8

typedef int ** Graphe;

Graphe creeGrapheJeu();
SDL_Rect* getCoordSommet();
SDL_Rect* CoordMatriceToCoordScreen(SDL_Rect * coordMatrice, int offset_x, int offset_y);
int videtoNonVide(int tabCase[][N*3], int x, int y);
void distanceAretes(Graphe graphe, SDL_Rect *pointRect);
void mainJeu();
void genMineral(Graphe graphe);
int Ramassage(SDL_Window* window, SDL_Renderer *renderer, SDL_Rect* rectSommet, Graphe graphe);
int verifClickSommet(SDL_Rect * rectSommet,int x,int y);
bool intInTab(int tab[],int taille,int val);
void genererHitBox(SDL_Rect rectSommet, SDL_Rect *rectHitBox);