#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "gestionJeu.h"

int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;
    mainJeu();
    return 0;
}
