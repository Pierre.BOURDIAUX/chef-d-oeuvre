#include "gestionJeu.h"


Graphe creeGrapheJeu(){
    
    float p = 1;
    Graphe matrice;


    matrice = creationMatrice(N);
    srand(time(NULL));
    matrice = genere(matrice, N,0, N - 1);

    matrice = genereGraphe(matrice,N, p);
    
    
    return matrice;
}
void mainJeu(){
    Graphe graphe;
    int window_w, window_h;
    int offset_x, offset_y;
    SDL_Rect * coordSommet;
    int ScoreJoueur = 0;
    int ScoreCOM = 0;

    graphe = creeGrapheJeu();

    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    initSDL(&window, &renderer);

    SDL_GetWindowSize(window, &window_w, &window_h);

    afficheFond(renderer);
    DrawPerso(renderer,100, 100);


    window_h -= 100;
    window_w -= 100;

    offset_x = window_w / (N*3);
    offset_y = window_h / (N*3);

    coordSommet = getCoordSommet();
    coordSommet = CoordMatriceToCoordScreen(coordSommet, offset_x, offset_y);

    distanceAretes(graphe, coordSommet);   
    genMineral(graphe);
    afficheMatrice(graphe,N);
    DrawMinerals(renderer, graphe,coordSommet, N);
    SDL_RenderPresent(renderer);
    ScoreJoueur = Ramassage(window, renderer, coordSommet, graphe);

    afficheResJoueur(renderer, window, ScoreJoueur);
    SDL_RenderClear(renderer);

    genMineral(graphe);
    afficheMatrice(graphe,N);
    afficheFond(renderer);
    DrawMinerals(renderer, graphe,coordSommet, N);

    SDL_RenderPresent(renderer);

    SDL_Delay(3000);
    ScoreCOM= COMPlay(graphe, N, renderer, coordSommet);
    afficheResCOM(renderer, window, ScoreCOM);


    SDL_Quit();
    freeMatrice(graphe,N);
}



SDL_Rect* getCoordSommet(){ // retourne un tableau de coordonnées de sommets
    SDL_Rect* coordSommet = malloc(N*sizeof(SDL_Rect));
    int nbChangement = 0;
    int tabCase[N*3][N*3];
    for (int i = 0; i < N*3; i++)
    {
        for (int j = 0; j < N*3; j++)
        {
            tabCase[i][j]=0;
        }
    }

    int random;
    for(int i=0;i<N;i++){
        random = rand()% ((3*N * 3*N)-nbChangement); // on choisit une case au hasard
        for(int j=0;j<N*3;j++){
            for(int k=0;k<N*3;k++){
                if(tabCase[j][k]==0){
                    random--;
                }
                if(random==0){
                   nbChangement+= videtoNonVide(tabCase,j,k); // on met la case en vide
                   coordSommet[i].x = j;
                   coordSommet[i].y = k;
                   coordSommet[i].w = 50;
                   coordSommet[i].h = 50;
                   j = k = N*3; // on sort des boucles


                }
            }
        }
    }

        return coordSommet;
}

SDL_Rect* CoordMatriceToCoordScreen(SDL_Rect * coordMatrice, int offset_x, int offset_y){
    for(int i=0;i<N;i++){
        coordMatrice[i].x = ((rand()%100)*0.01) * offset_x + offset_x*coordMatrice[i].x + 50; // on ajoute 50 pour que les sommets ne soient pas collés au bord
        coordMatrice[i].y =((rand()%100)*0.01) * offset_y + offset_y*coordMatrice[i].y + 50; 

    }
    return coordMatrice;
}



int videtoNonVide(int tabCase[][N*3], int x, int y){ // on met la case et ses voisins en vide et on renvoie le nombre de changement
    int nbChangement=1; //la case est forcément libre au début
    tabCase[x][y]=2;

    if(x>0){
        if(tabCase[x-1][y]==0){
            nbChangement++;
            tabCase[x-1][y]=1;
        }
    }
    if(x<N*3-1){
         if(tabCase[x+1][y]==0){
            nbChangement++;
            tabCase[x+1][y]=1;
        }
    }
    if(y>0){
        if(tabCase[x][y-1]==0){
            nbChangement++;
            tabCase[x][y-1]=1;
        }

    }
    if(y<N*3-1){
        if(tabCase[x][y+1]==0){
            nbChangement++;
            tabCase[x][y+1]=1;
        }

    }
    if(x>0 && y>0){
        if(tabCase[x-1][y-1]==0){
            nbChangement++;
            tabCase[x-1][y-1]=1;
        }
    }
    if(x>0 && y<N*3-1){
        if(tabCase[x-1][y+1]==0){
            nbChangement++;
            tabCase[x-1][y+1]=1;
        }
    }
    if(x<N*3-1 && y>0){
        if(tabCase[x+1][y-1]==0){
            nbChangement++;
            tabCase[x+1][y-1]=1;
        }
    }
    if(x<N*3-1 && y<N*3-1){
        if(tabCase[x+1][y+1]==0){
            nbChangement++;
            tabCase[x+1][y+1]=1;
        }
    }
    return nbChangement;

}

void distanceAretes(Graphe graphe, SDL_Rect *pointRect){

    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
           if(graphe[i][j]!=0){
            graphe[i][j]=sqrt(pow(pointRect[i].x-pointRect[j].x,2)+pow(pointRect[i].y-pointRect[j].y,2));
            graphe[j][i]=graphe[i][j];
           }
        }
    }
}

void genMineral(Graphe graphe){
    srand(time(NULL));
    int nbMin1 = N/3;
    int nbMin2 = N/3;
    int nbMin3 = N - nbMin1-nbMin2;
    int i = 0;
    for(int j=0;j<nbMin1;j++){
        graphe[i][i] = 1;
        i++;
    }
    for(int j=0;j<nbMin2;j++){
        graphe[i][i] = 2;
        i++;
    }
    for(int j=0;j<nbMin3;j++){
        graphe[i][i] = 3;
        i++;
    }

}

int Ramassage(SDL_Window* window, SDL_Renderer *renderer, SDL_Rect* rectSommet, Graphe graphe){
 
    SDL_Rect rectPerso = {100,100,250,250};
    SDL_Rect rectHitboxSommet[N];
    for(int i=0;i<N;i++){
        genererHitBox(rectSommet[i],&rectHitboxSommet[i]);
    }
    SDL_Rect rectHitboxPerso;
    genererHitBox(rectPerso,&rectHitboxPerso);
    int nbRammassage = 0;
    int Score = 0; //  nombre de pixel parcourue
    int premierSommet = -1;
    SDL_bool
      program_on = SDL_TRUE;                         

    int vitesseX = 0;
    int vitesseY = 0;

    // Dans la boucle principale du jeu
    while (program_on)
    {
        // Gestion des événements
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_UP:
                            printf("UP\n");
                            vitesseY = -10;
                            break;
                        case SDLK_DOWN:
                            vitesseY = 10;
                            break;
                        case SDLK_RIGHT:
                            vitesseX = 10;
                            break;
                        case SDLK_LEFT:
                            vitesseX = -10;
                            break;
                    }
                    break;

                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_UP:
                        case SDLK_DOWN:
                            vitesseY = 0;
                            break;
                        case SDLK_RIGHT:
                        case SDLK_LEFT:
                            vitesseX = 0;
                            break;
                    }
                    break;

                case SDL_QUIT:
                    program_on = false;
                    break;
            }
        }

        // Mise à jour de la position du personnage
        printf("x : %d y : %d\n",rectPerso.x,rectPerso.y);
        printf("vitesseX : %d vitesseY : %d\n",vitesseX,vitesseY);
        rectPerso.x += vitesseX;
        rectPerso.y += vitesseY;

        if(rectPerso.x < 0) rectPerso.x = 0;
        if(rectPerso.y < 0) rectPerso.y= 0;
        if(rectPerso.x > 1280-188) rectPerso.x = 1280-188; // 188 = largeur du personnage 
        if(rectPerso.y > 720-275) rectPerso.y = 720-275; // 275 = hauteur du personnage

        if(nbRammassage!=0){
            Score += abs(vitesseX)+abs(vitesseY);
            printf("Score : %d\n",Score);
        }
        genererHitBox(rectPerso,&rectHitboxPerso);

        // Autres opérations de mise à jour du jeu...

        for(int i=0;i<N;i++){

            if(SDL_HasIntersection(&rectHitboxPerso,&rectHitboxSommet[i])){
                if(graphe[i][i]!=-1){
                    if(nbRammassage==0){
                        premierSommet = i;
                    }
                    nbRammassage++;
                }
                graphe[i][i]=-1;
            }
        }

        // Affichage du personnage...
        SDL_RenderClear(renderer);
        afficheFond(renderer);
        DrawMinerals(renderer,graphe,rectSommet,N);
        DrawPerso(renderer,rectPerso.x,rectPerso.y);
        SDL_RenderPresent(renderer);

        if(nbRammassage==N){
            program_on = false;
        }
        
    }
    printf( "Vous avez ramassé tous les minéraux !\n");
    printf( "Votre score est de %d\n",Score);
    printf("Premier sommet : %d\n",premierSommet);

    return Score;


}




int verifClickSommet(SDL_Rect * rectSommet,int x,int y){
    printf("x : %d y : %d\n",x,y);
    int numSommet = -1;
    for (int i = 0; i < N; i++)
    {
        if(x>rectSommet[i].x && x<rectSommet[i].x+rectSommet[i].w && y>rectSommet[i].y && y<rectSommet[i].y+rectSommet[i].h){
            numSommet = i;
            break;
        }
    }
    return numSommet;
}
bool intInTab(int tab[],int taille,int val){
    bool res = false;
    for (int i = 0; i < taille; i++)
    {
        if(tab[i]==val){
            res = true;
            break;
        }
    }
    return res;
}

void genererHitBox(SDL_Rect rectSommet, SDL_Rect *rectHitBox){
    rectHitBox->x = rectSommet.x+rectSommet.w/4;
    rectHitBox->y = rectSommet.y+rectSommet.h/4;
    rectHitBox->w = rectSommet.w/2.5;
    rectHitBox->h = rectSommet.h/1.5;
    
}