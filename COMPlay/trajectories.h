#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


typedef struct 
{
    int modx;
    int mody;
} trajectory;

trajectory getTrajectory(int x1 , int y1 , int x2, int y2);
void pickMineral(SDL_Renderer * renderer, SDL_Rect * coordMineral, int startMineral, int destMineral);