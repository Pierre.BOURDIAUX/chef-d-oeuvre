#include "trajectories.h"
#include "../GenGraphe/creation_graphe.h"
#include "../recuit_simule/recuit_simulé.h"
#include "../ParcoursGraphe/floyd_warshall.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


int COMPlay(int ** graphe, int taille,SDL_Renderer * renderer,SDL_Rect * coordSommet)
{
    etat * etat_optimal = simulatedAnnealing(graphe,taille,100000,calculPoids);
    printf("Poids optimal : %d\n",etat_optimal->poids);
    printf("Trajet optimal : ");
    int nbSommetVisite = 1;
    float x,y;
    for (int i = 0; i < taille; i++)
    {
        printf("%d",etat_optimal->liste_sommet[i]);
        if (i < taille - 1)
        {
            printf(", ");
        }
    }
    printf("\n");

    SDL_Rect rectPerso;

    rectPerso.w = 250;
    rectPerso.h = 250;

    SDL_Rect rectHitboxPerso;
    genererHitBox(rectPerso,&rectHitboxPerso);

    SDL_Rect rectHitboxSommet[taille];
    for(int i=0;i<taille;i++){
        genererHitBox(coordSommet[i],&rectHitboxSommet[i]);
    }  


    int score=0;
    for(int i = 0; i<taille; i++){

        rectPerso.x = coordSommet[etat_optimal->liste_sommet[i]].x-100;
        rectPerso.y = coordSommet[etat_optimal->liste_sommet[i]].y -100;
        graphe[etat_optimal->liste_sommet[i]][etat_optimal->liste_sommet[i]]=-1;

        SDL_RenderClear(renderer);
        afficheFond(renderer);
        DrawMinerals(renderer,graphe,coordSommet,taille);
        DrawPerso(renderer,rectPerso.x,rectPerso.y);
        SDL_RenderPresent(renderer);

        if(i>0){
            score += graphe[etat_optimal->liste_sommet[i-1]][etat_optimal->liste_sommet[i]] - 100; // on enleve 100 pour simuler une hitbox
        }
        

        for(int j = 0; j<taille; j++){
            printf("%d ",graphe[j][j]);
            printf("\n");
        }
        SDL_Delay(1000);

    }
    printf("Score : %d\n", score);

    

    SDL_Delay(1000);
    return score;
}


